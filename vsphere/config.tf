provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
  version="~> 1.24"
}

data "vsphere_datacenter" "dc" {
  name = "SA-Datacenter"
}

data "vsphere_datastore" "datastore" {
  name          = "SA-Shared-01"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_compute_cluster" "cluster" {
  name          = "SA-Compute-02"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = "SA-Production"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "CentOS-Template"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
  guest_id = "centos7_64Guest"
}
